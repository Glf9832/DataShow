#_*_coding:utf-8_*_

import os

basedir = os.path.dirname(os.path.realpath(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'

    MAIL_SERVER = 'smtp.qq.com'   #电子邮件服务器的主机名或IP地址
    MAIL_PORT = 25          #电子邮件服务器的端口
    MAIL_USE_TLS = True     #启用传输层安全协议TLS
    MAIL_USE_SSL = False    #启用安全套接层协议SSL
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or '2540669209@qq.com'      # From邮件账户的用户名
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or 'zizdtgsbhqzpdhhg'            # From邮件账户的密码
    FLASKY_MAIL_SUBJECT_PREFIX = '[DataShow]'
    FLASKY_MAIL_SENDER = '2540669209@qq.com'
    # FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfigMysql(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'mysql://root:mchip.13579@192.168.1.123/datamanage'

class DevelopmentConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir,'data-test.sqlite')

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir,'data.sqlite')

config = {
    'developmentmysql':DevelopmentConfigMysql,
    'development':DevelopmentConfig,
    'testing':TestingConfig,
    'production':ProductionConfig,
    'default':DevelopmentConfig
}

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017