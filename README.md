![](https://img.shields.io/badge/python-3.5%2B-blue.svg)
![](https://img.shields.io/badge/Flask-0.12.2-green.svg)
![](https://img.shields.io/badge/build-passing-brightgreen.svg)
![](https://img.shields.io/badge/License-GPL%20v3-blue.svg)  

# DataShow
