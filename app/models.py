#_*_coding:utf-8_*_

from . import db
from werkzeug.security import generate_password_hash,check_password_hash
from . import login_manager
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app

class Role(db.Model):
    """用户角色"""
    __tablename__ = 'tb_roles'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(64),unique=True)
    users = db.relationship('User',backref='role')

    def __repr__(self):
        return '<Role %r>'%self.name

class User(db.Model):
    """用户"""
    __tablename__ = 'tb_users'
    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64),unique=True,index=True)
    role_id = db.Column(db.Integer,db.ForeignKey('tb_roles.id'))
    confirmed = db.Column(db.Boolean,default=False)

    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self,password):
        self.password_hash = generate_password_hash(password)
        # print 'password_hash>>>%s' % self.password_hash

    def verify_password(self,password):
        # print 'password_hash>>>%s'%self.password_hash
        return check_password_hash(self.password_hash,password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def generate_confirmation_token(self,expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'],expiration)
        return s.dumps({'confirm':self.id})

    def confirm(self,token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def __repr__(self):
        return '<User %r>'%self.username

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class StatTime(db.Model):
    __tablename__ = 'tb_stattime'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32),unique=True,index=True)

class Ave(db.Model):
    '''均价'''
    __tablename__ = 'tb_ave'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32),unique=True,index=True)
    bjave = db.Column(db.Float)
    shave = db.Column(db.Float)
    szave = db.Column(db.Float)
    gzave = db.Column(db.Float)
    hzave = db.Column(db.Float)
    dgave = db.Column(db.Float)
    huizave = db.Column(db.Float)
    fsave = db.Column(db.Float)
    zhave = db.Column(db.Float)

class MaxPrice(db.Model):
    '''最高价'''
    __tablename__ = 'tb_maxprice'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32),unique=True,index=True)
    bjmaxprice = db.Column(db.Float)
    shmaxprice = db.Column(db.Float)
    szmaxprice = db.Column(db.Float)
    gzmaxprice = db.Column(db.Float)
    hzmaxprice = db.Column(db.Float)
    dgmaxprice = db.Column(db.Float)
    huizmaxprice = db.Column(db.Float)
    fsmaxprice = db.Column(db.Float)
    zhmaxprice = db.Column(db.Float)

class MinPrice(db.Model):
    '''最低价'''
    __tablename__ = 'tb_minprice'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32),unique=True,index=True)
    bjminprice = db.Column(db.Float)
    shminprice = db.Column(db.Float)
    szminprice = db.Column(db.Float)
    gzminprice = db.Column(db.Float)
    hzminprice = db.Column(db.Float)
    dgminprice = db.Column(db.Float)
    huizminprice = db.Column(db.Float)
    fsminprice = db.Column(db.Float)
    zhminprice = db.Column(db.Float)

class SampleSize(db.Model):
    '''统计样本量'''
    __tablename__ = 'tb_samplesize'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32),unique=True,index=True)
    bjsize = db.Column(db.Integer)
    shsize = db.Column(db.Integer)
    szsize = db.Column(db.Integer)
    gzsize = db.Column(db.Integer)
    hzsize = db.Column(db.Integer)
    dgsize = db.Column(db.Integer)
    huizsize = db.Column(db.Integer)
    fssize = db.Column(db.Integer)
    zhsize = db.Column(db.Integer)

class ServerInfo(db.Model):
    '''服务器实时信息'''
    __tablename__ = 'tb_serverinfo'
    id = db.Column(db.Integer,primary_key=True)
    time = db.Column(db.String(32))
    memory = db.Column(db.Float)
    cpu1 = db.Column(db.Float)
    cpu2 = db.Column(db.Float)
    cpu3 = db.Column(db.Float)
    cpu4 = db.Column(db.Float)


