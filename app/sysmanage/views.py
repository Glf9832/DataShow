#_*_coding:utf-8_*_

from datetime import datetime
from threading import Lock
from flask import render_template,request
from .. import socketio
from . import serverinfo
from ..models import User
from app import que
from ..fangdata.dataanalysis import DataAnalysis

thread = None
thread_lock = Lock()

def background_thread():
    while True:
        socketio.sleep(0.5)
        si = que.get()
        for s in si[::-1]:
            socketio.emit('server_response',
                          {'data': [s.time, s.cpu1, s.cpu2, s.cpu3, s.cpu4, s.memory],},
                          namespace='/test')

@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread)

@serverinfo.route('/servermanage/')
def BgManage():
    return render_template('manage/servermanage.html',timesize=30,year=datetime.now().year, async_mode=socketio.async_mode)

@serverinfo.route('/bgmanage/')
@serverinfo.route('/usermanage/')
def Usermanage():
    users = User.query.all()
    return render_template('manage/usermanage.html',year=datetime.now().year,users=users)

@serverinfo.route('/crawlermanage/',methods=['GET','POST'])
def Crawlermanage():
    if request.method == 'POST':
        DataAnalysis()
    return render_template('manage/crawlermanage.html',year=datetime.now().year)