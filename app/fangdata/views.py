#_*_coding:utf-8_*_

from flask import render_template,request
from . import fang
from .dataanalysis import areas
from .forms import SelectForm
from ..models import Ave,MaxPrice,MinPrice,SampleSize,StatTime
from datetime import datetime

@fang.route('/fangdata/',methods=['GET','POST'])
def GetFangData():
    selectForm = SelectForm()
    choices = []
    index = 0
    stattimes = StatTime.query.all()
    for col in stattimes:
        choice = (index,col.time)
        choices.append(choice)
        index+=1

    selectForm.selectcols.choices = choices
    ch = [i[1] for i in choices]
    if request.method == 'POST':
        select = selectForm.selectcols.data-1
        collection = choices[select][1]
        areasmeans, areasmaxs, areasmins, areascount,stattimeall,bjaveall,shaveall,szaveall,gzaveall,hzaveall,dgaveall,huizaveall,fsaveall,zhaveall = getValue(collection)
        # print (type(stattimeall))
        return render_template('fangdata.html',
                               choices=ch,
                               collection=collection,
                               areas=areas,
                               areasmeans=areasmeans,
                               areasmaxs=areasmaxs,
                               areasmins=areasmins,
                               areascount=areascount,
                               stattimeall=stattimeall,
                               bjaveall=bjaveall,
                               shaveall=shaveall,
                               szaveall=szaveall,
                               gzaveall=gzaveall,
                               hzaveall=hzaveall,
                               dgaveall=dgaveall,
                               huizaveall=huizaveall,
                               fsaveall=fsaveall,
                               zhaveall=zhaveall,
							   year=datetime.now().year)
    areasmeans, areasmaxs, areasmins, areascount,stattimeall,bjaveall,shaveall,szaveall,gzaveall,hzaveall,dgaveall,huizaveall,fsaveall,zhaveall = getValue(choices[0][1])
    return render_template('fangdata.html',
                           choices=ch,
                               collection=choices[0][1],
                               areas=areas,
                               areasmeans=areasmeans,
                               areasmaxs=areasmaxs,
                               areasmins=areasmins,
                               areascount=areascount,
                               stattimeall=stattimeall,
                               bjaveall=bjaveall,
                               shaveall=shaveall,
                               szaveall=szaveall,
                               gzaveall=gzaveall,
                               hzaveall=hzaveall,
                               dgaveall=dgaveall,
                               huizaveall=huizaveall,
                               fsaveall=fsaveall,
                               zhaveall=zhaveall,
							   year=datetime.now().year)

def getValue(collection):
    ave = Ave.query.filter_by(time=collection)[0]
    maxprice = MaxPrice.query.filter_by(time=collection)[0]
    minprice = MinPrice.query.filter_by(time=collection)[0]
    samplesize = SampleSize.query.filter_by(time=collection)[0]

    aveall = Ave.query.all()
    bjaveall = [ave.bjave for ave in aveall]
    shaveall = [ave.shave for ave in aveall]
    szaveall = [ave.szave for ave in aveall]
    gzaveall = [ave.gzave for ave in aveall]
    hzaveall = [ave.hzave for ave in aveall]
    dgaveall = [ave.dgave for ave in aveall]
    huizaveall = [ave.huizave for ave in aveall]
    fsaveall = [ave.fsave for ave in aveall]
    zhaveall = [ave.zhave for ave in aveall]

    stattime = StatTime.query.all()
    stattimeall = [stat.time for stat in stattime]
    #print stattimeall

    aves = []
    maxprices = []
    minprices = []
    samplesizes = []

    aves.append(ave.bjave)
    aves.append(ave.shave)
    aves.append(ave.szave)
    aves.append(ave.gzave)
    aves.append(ave.hzave)
    aves.append(ave.dgave)
    aves.append(ave.huizave)
    aves.append(ave.fsave)
    aves.append(ave.zhave)

    maxprices.append(maxprice.bjmaxprice)
    maxprices.append(maxprice.shmaxprice)
    maxprices.append(maxprice.szmaxprice)
    maxprices.append(maxprice.gzmaxprice)
    maxprices.append(maxprice.hzmaxprice)
    maxprices.append(maxprice.dgmaxprice)
    maxprices.append(maxprice.huizmaxprice)
    maxprices.append(maxprice.fsmaxprice)
    maxprices.append(maxprice.zhmaxprice)

    minprices.append(minprice.bjminprice)
    minprices.append(minprice.shminprice)
    minprices.append(minprice.szminprice)
    minprices.append(minprice.gzminprice)
    minprices.append(minprice.hzminprice)
    minprices.append(minprice.dgminprice)
    minprices.append(minprice.huizminprice)
    minprices.append(minprice.fsminprice)
    minprices.append(minprice.zhminprice)

    samplesizes.append(samplesize.bjsize)
    samplesizes.append(samplesize.shsize)
    samplesizes.append(samplesize.szsize)
    samplesizes.append(samplesize.gzsize)
    samplesizes.append(samplesize.hzsize)
    samplesizes.append(samplesize.dgsize)
    samplesizes.append(samplesize.huizsize)
    samplesizes.append(samplesize.fssize)
    samplesizes.append(samplesize.zhsize)

    return aves,maxprices,minprices,samplesizes,stattimeall,bjaveall,shaveall,szaveall,gzaveall,hzaveall,dgaveall,huizaveall,fsaveall,zhaveall

