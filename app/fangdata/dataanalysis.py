#_*_coding:utf-8_*_

import pymongo
import pandas as pd
import config
from ..models import Ave,MaxPrice,MinPrice,SampleSize,StatTime
from app import db
from sklearn import preprocessing
from sklearn.preprocessing import Imputer

areas = [u'北京', u'上海', u'深圳', u'广州', u'杭州', u'东莞', u'惠州', u'佛山', u'珠海']

class DataAnalysis(object):
    def __init__(self):
        client = pymongo.MongoClient(config.MONGODB_HOST, config.MONGODB_PORT)
        self.fangdb = client['fangdb']
        collections = self.fangdb.collection_names()
        if 'system.indexes' in collections:
            collections.remove('system.indexes')
        self.collections = collections
        self.DBinit()

    def __GetDataResult(self,collection,table):
        newfanginfo = self.fangdb[collection]
        df = pd.DataFrame(list(newfanginfo.find()))
        df1 = df.loc[:, ['EstateArea', 'RefPrice', 'AvePrice', 'HuXing']]

        areasmeans = []
        areasmaxs = []
        areasmins = []
        areascount = []
        for i in range(0, len(areas)):
            areasdf = df1.loc[df['EstateArea'].isin([areas[i]])]
            # areasave = areasdf['RefPrice'] * 10000 / areasdf['HuXing']
            areasave = areasdf['AvePrice']
            areasave = pd.DataFrame({'AvePrice': areasave.tolist()})
            nan_model = Imputer(missing_values='NaN', strategy='mean', axis=0)
            if len(areasave) == 0:
                return
            nan_result = nan_model.fit_transform(areasave)
            avelist = []
            for nr in nan_result:
                avelist.append(nr[0])

            aves = []
            for ave in avelist:
                if ave>2000 and ave<100000:
                    aves.append(ave)

            aves = pd.DataFrame({'AvePrice': aves})
            zscore_scaler = preprocessing.StandardScaler()
            data_scale = zscore_scaler.fit_transform(aves)
            for i in range(0, len(data_scale)):
                if data_scale[i][0] > 1.5:
                    adfave = aves.drop(i)

            areasmean = float('%.1f'%adfave.mean())
            areasmax = float('%.1f'%adfave.max())
            areasmin = float('%.1f'%adfave.min())
            areasmeans.append(areasmean)
            areasmaxs.append(areasmax)
            areasmins.append(areasmin)
            areascount.append(len(adfave))

        if table==Ave:
            ave = Ave(time=collection,
                      bjave=areasmeans[0],
                      shave=areasmeans[1],
                      szave=areasmeans[2],
                      gzave=areasmeans[3],
                      hzave=areasmeans[4],
                      dgave=areasmeans[5],
                      huizave=areasmeans[6],
                      fsave=areasmeans[7],
                      zhave=areasmeans[8], )
            db.session.add_all([ave])
            db.session.commit()

        if table==MaxPrice:
            maxprice = MaxPrice(time=collection,
                                bjmaxprice=areasmaxs[0],
                                shmaxprice=areasmaxs[1],
                                szmaxprice=areasmaxs[2],
                                gzmaxprice=areasmaxs[3],
                                hzmaxprice=areasmaxs[4],
                                dgmaxprice=areasmaxs[5],
                                huizmaxprice=areasmaxs[6],
                                fsmaxprice=areasmaxs[7],
                                zhmaxprice=areasmaxs[8])
            db.session.add_all([maxprice])
            db.session.commit()

        if table==MinPrice:
            minprice = MinPrice(time=collection,
                                bjminprice=areasmins[0],
                                shminprice=areasmins[1],
                                szminprice=areasmins[2],
                                gzminprice=areasmins[3],
                                hzminprice=areasmins[4],
                                dgminprice=areasmins[5],
                                huizminprice=areasmins[6],
                                fsminprice=areasmins[7],
                                zhminprice=areasmins[8])
            db.session.add_all([minprice])
            db.session.commit()

        if table==SampleSize:
            samplesize = SampleSize(time=collection,
                      bjsize=areascount[0],
                      shsize=areascount[1],
                      szsize=areascount[2],
                      gzsize=areascount[3],
                      hzsize=areascount[4],
                      dgsize=areascount[5],
                      huizsize=areascount[6],
                      fssize=areascount[7],
                      zhsize=areascount[8], )
            db.session.add_all([samplesize])
            db.session.commit()

        if table == StatTime:
            stattime = StatTime(time=collection)
            db.session.add_all([stattime])
            db.session.commit()

    def DBinit(self):
        for col in self.collections:
            avetimes = Ave.query.filter_by(time=col).all()
            maxpricetimes = MaxPrice.query.filter_by(time=col).all()
            minpricetimes = MinPrice.query.filter_by(time=col).all()
            samplesizetimes = SampleSize.query.filter_by(time=col).all()
            stattimes = StatTime.query.filter_by(time=col).all()
            if len(avetimes) == 0:
                self.__GetDataResult(col,Ave)
            if len(maxpricetimes) == 0:
                self.__GetDataResult(col,MaxPrice)
            if len(minpricetimes) == 0:
                self.__GetDataResult(col,MinPrice)
            if len(samplesizetimes) == 0:
                self.__GetDataResult(col,SampleSize)
            if len(stattimes) == 0:
                self.__GetDataResult(col,StatTime)














