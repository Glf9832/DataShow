#_*_coding:utf-8_*_

from flask_wtf import FlaskForm
from wtforms import SelectField,SubmitField,validators


class SelectForm(FlaskForm):
    selectcols = SelectField(u'',validators=[validators.Optional()], choices=[], coerce=int)
    submit = SubmitField()
