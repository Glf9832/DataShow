# -*- coding:utf-8 -*-

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_login import LoginManager
from flask_socketio import SocketIO
from config import config
import threading
import time
import psutil
from queue import Queue

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()
api = Api()
socketio = SocketIO()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

app = None
que = Queue()

def create_app(config_name):
    global app
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    api.init_app(app)
    login_manager.init_app(app)
    socketio.init_app(app)

    from .main import main as main_blueprint
    from .apis import api as api_blueprint
    from .auth import auth as auth_blueprint
    from .fangdata import fang as fang_blueprint
    from .sysmanage import serverinfo as serverinfo_blueprint

    app.register_blueprint(main_blueprint)
    app.register_blueprint(api_blueprint)
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(fang_blueprint)
    app.register_blueprint(serverinfo_blueprint)

    getserver_thread = threading.Thread(target=getserverfun,args=(app,), name='test_thread')  #Debug模式下会创建两次
    getserver_thread.start()

    return app

def getserverfun(application):
    from app import db
    from .models import ServerInfo

    global que

    while True:
        time.sleep(1)
        t = time.strftime('%H:%M:%S', time.localtime())
        cpus = psutil.cpu_percent(interval=None, percpu=True)
        memory = psutil.virtual_memory().percent
        # data = {'time':t,'memory':memory,'cpu':cpus}

        with application.app_context():
            count = ServerInfo.query.count()
            if count >=60*60*24*7:
                d = ServerInfo.query.all()[0]
                db.session.delete(d)
                db.session.commit()

            serverinfo = ServerInfo(time=t,memory=memory,cpu1=cpus[0])
            db.session.add(serverinfo)
            db.session.commit()

            si = ServerInfo.query.order_by(ServerInfo.id.desc()).limit(32).all()
            que.put(si)



