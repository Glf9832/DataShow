#_*_coding:utf-8_*_

from . import api
from flask import jsonify,request,url_for
import json
from ..models import User
from flask_login import login_user,login_required
from app import db

@api.app_errorhandler(404)
def page_not_found(e):
    return jsonify({'error':str('404')})

@api.app_errorhandler(500)
def internal_server_error(e):
    return jsonify({'error':str('500')})

@api.route('/api/login/',methods=['GET','POST'])
def login():
    loginstate = False
    if request.method == 'POST':
        jsondata = json.loads(request.data)
        user = User.query.filter_by(email=jsondata['Email']).first()
        if user is not None and user.verify_password(jsondata['Pwd']):
            login_user(user,bool(jsondata['IsRememberPwd']))
            loginstate = True
        else:
            loginstate = False

    link = {
        'href': url_for('api.login', _external=True),
        'productinfo_href':url_for('api.TProductinfo', _external=True),
        'substratePro_href': url_for('api.TSubstratePro', _external=True),
        'mts_href': url_for('api.TMts', _external=True),
        'submaterial_href': url_for('api.TSubMaterial', _external=True),
        'scrubpro_href': url_for('api.TScrubPro', _external=True),
        'drying_href': url_for('api.TDrying', _external=True),
        'title': u'login',
        'version': 0.1,
    }
    state = {
        'loninstate':loginstate,
    }
    return jsonify({'link':link,'state':state})

@api.route('/api/logout/')
@login_required
def logout():
    login_user()
    return u"用户退出!"

@api.route('/api/register/',methods=['GET','POST'])
def register():
    if request.method == 'POST':
        jsondata = json.loads(request.data)
        user = User(email=jsondata['Email'], username=jsondata['Name'], password=jsondata['Pwd'])
        db.session.add(user)
        db.session.commit()
        return 'True'
    else:
        return 'False'
