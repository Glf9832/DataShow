#_*_coding:utf-8_*_

from flask_wtf import FlaskForm
from wtforms import StringField,SubmitField,PasswordField,BooleanField
from wtforms.validators import DataRequired,Required,Length,Email

class LoginForm(FlaskForm):
    email = StringField(u'邮箱',validators=[DataRequired(),Length(1,64),Email()])
    password = PasswordField('Password',validators=[DataRequired()])
    remember_me = BooleanField(u'记住密码')
    submit = SubmitField(u'登陆')



