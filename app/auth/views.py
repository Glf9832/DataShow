#_*_coding:utf-8_*_

from flask import render_template,redirect,url_for,flash,request
from . import auth
from flask_login import login_user,logout_user,login_required,current_user
from ..models import User
from app import db
from datetime import datetime
from ..email import send_email

@auth.route('/login/',methods=['GET','POST'])
def login():
    # form = LoginForm()
    if request.method == 'POST':
        username = request.form['Username']
        password = request.form['Password']
        #rememberme = request.form['Rememberme']
        user = User.query.filter_by(email=username).first()
        if not user:
            return render_template('auth/login.html')
        if username is not None and user.verify_password(password):
            #print 'rememberme >>>>>>',rememberme
            login_user(user)
            # send_email(user.email,'Confirm Your Account','auth/confirm',user=user)
            return render_template('index.html',year=datetime.now().year)
        # flash(u'请填写正确邮箱或密码！')
        return redirect(url_for('auth.login'))
    else:
        return render_template('auth/login.html')

@auth.route('/logout/')
@login_required
def logout():
    logout_user()
    return render_template('index.html')

@auth.route('/register/',methods=['GET','POST'])
def register():
    if request.method == 'POST':
        email = request.form['Email']
        username = request.form['UserName']
        password = request.form['PassWord']
        confirmpassword = request.form['ConfirmPassword']
        if password == confirmpassword and username is not None and email is not None:
            user = User(email=email,
                        username=username,
                        password=password)
            try:
                db.session.add(user)
                db.session.commit()
                token = user.generate_confirmation_token()
                send_email(user.email,'验证你的邮箱','auth/email/confirm',user=user,token=token)
            except:
                db.session.rollback()
                flash(u"提示：邮箱或账号已经注册过")
                return render_template('auth/register.html')
            # flash('You can now login.')
            # send_email(user.email, '用户注册', 'auth/confirm', user=user)
            return redirect(url_for('auth.login'))
    return render_template('auth/register.html')

@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('main.home'))
    if current_user.confirm(token):
        db.session.commit()
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('main.home'))

@auth.before_app_request
def before_request():
    if current_user.is_authenticated \
            and not current_user.confirmed \
            and request.blueprint !='auth'\
            and request.endpoint != 'static':
        return redirect(url_for('auth.unconfirmed'))

@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous() or current_user.confirmed:
        return redirect(url_for('main.home'))
    return render_template('auth/unconfirmed.html')

@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email(current_user.email,'Confirm Your Account',
               'auth/email/confirm',user=current_user,token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.home'))





